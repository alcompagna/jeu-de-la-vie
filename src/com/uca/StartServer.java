package com.uca;

import com.uca.dao._Initializer;
import com.uca.dao._Connector;
import com.uca.dao.GridDAO;
import com.uca.gui.*;

import com.uca.core.GridCore;
import com.uca.entity.CellEntity;
import com.uca.core.CellCore;

import com.google.gson.Gson;

import static spark.Spark.*;
import spark.*;

public class StartServer {

    public static void main(String[] args) {
        //Configuration de Spark
        staticFiles.location("/static/");
        port(8081);

        // Création de la base de données, si besoin
        _Initializer.Init();

        /**
         * Définition des routes
         */

        // index de l'application
        get("/", (req, res) -> {
                return IndexGUI.getIndex();
            });

        // retourne l'état de la grille
        get("/grid", (req, res) -> {
                int session = getSession(req);
                res.type("application/json");
                // while (_Connector.isSessionLocked(session)) {}
                // _Connector.lockSession(session);
                return new Gson().toJson(GridCore.getGrid(session));
            });

        // inverse l'état d'une cellule 
        put("/grid/change", (req, res) -> {
                Gson gson = new Gson();
                CellEntity selectedCell = (CellEntity) gson.fromJson(req.body(), CellEntity.class);
                int session = getSession(req);

                // tant qu'une session bloque les modifs
                // while (_Connector.isSessionLocked(session)) {}
                // _Connector.lockSession(session); // On bloque avec cette session
                
                CellCore.changeStateCell(session, selectedCell);
                return "";
            });

        // sauvegarde les modifications de la grille 
        post("/grid/save", (req, res) -> {
                GridCore.save(getSession(req));
                return "";
            });

        // annule les modifications de la grille 
        post("/grid/cancel", (req, res) -> {
                GridCore.cancel(getSession(req));
                return "";
            });

        // charge un fichier rle depuis un URL
        put("/grid/rle", (req, res) -> {
                int session = getSession(req);
                // while (_Connector.isSessionLocked(session)) {}
                // _Connector.lockSession(session);
                String RLEUrl = req.body();
                GridCore.getRle(session, RLEUrl);
                
                return "";
            });

        // vide la grille
        post("/grid/empty", (req, res) -> {
                int session = getSession(req);
                // while (_Connector.isSessionLocked(session)) {}
                // _Connector.lockSession(session);
                GridCore.emptyGrid(session);
                return "";
            });

        // met à jour la grille en la remplaçant par la génération suivante
        post("/grid/next", (req, res) -> {
                int session = getSession(req);
                // while (_Connector.isSessionLocked(session)) {}
                // _Connector.lockSession(session);
                GridCore.next(session);
                return "";
            });

    }

    /**
     * retourne le numéro de session
     * il y a un numéro de session différent pour chaque onglet de navigateur
     * ouvert sur l'application
     */
    public static int getSession(Request req) {
        return Integer.parseInt(req.queryParams("session"));
    }
}
