package com.uca.dao;

import java.sql.*;
import java.util.List;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;

import com.uca.entity.CellEntity;
import com.uca.entity.GridEntity;
import com.uca.dao._Connector;

public class GridDAO {

    public List<CellEntity> getGrid(int session) throws SQLException{
        Connection connection = _Connector.getConnection(session);
        try {
            GridEntity grid = new GridEntity();
            PreparedStatement preparedStatement = connection.prepareStatement(
                "SELECT * FROM grid WHERE state = ?;"
            );
            preparedStatement.setBoolean(1, true);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                // On ajoute toutes les cellules vivantes à la grille 
                CellEntity cell = new CellEntity(resultSet.getInt("x"), resultSet.getInt("y"));
                grid.addCell(cell);
            }
            return grid.getCells(); // On renvoie le tableau de cellules vivantes 
        } catch (SQLException e) { e.printStackTrace(); connection.rollback(); return null;}
        
    } 


    public void next(int session) throws SQLException {
        Connection connection = _Connector.getConnection(session);
        try {
            PreparedStatement preparedStatement;

            preparedStatement = connection.prepareStatement(
                "WITH alive_cells AS ( " +
                "SELECT x, y FROM grid " + 
                "WHERE state = true )" +
                "SELECT grid.x, grid.y FROM grid "
                +"INNER JOIN alive_cells " +
                "ON (grid.x >= alive_cells.x - 1 AND grid.x <= alive_cells.x + 1 AND grid.y >= alive_cells.y - 1 AND grid.y <= alive_cells.y + 1 AND NOT (grid.x = alive_cells.x AND grid.y = alive_cells.y)) " +
                "WHERE grid.state = true " +
                "GROUP BY grid.x, grid.y " +
                "HAVING COUNT(*) = 2 OR COUNT(*) = 3 "
                );

            ResultSet resultSet = preparedStatement.executeQuery();

            List<SimpleEntry<Integer, Integer>> aliveCoordinates = new ArrayList<>();

            while (resultSet.next()) {
                int x = resultSet.getInt("x");
                int y = resultSet.getInt("y");
                aliveCoordinates.add(new SimpleEntry<>(x, y));
            }

            preparedStatement = connection.prepareStatement(
            "WITH alive_cells AS (SELECT x, y FROM grid WHERE state = true) " +
            "SELECT grid.x, grid.y " +
            "FROM grid " +
            "INNER JOIN alive_cells " +
            "ON (grid.x >= alive_cells.x - 1 AND grid.x <= alive_cells.x + 1 AND grid.y >= alive_cells.y - 1 AND grid.y <= alive_cells.y + 1) " +
            "WHERE grid.state = false " +
            "GROUP BY grid.x, grid.y " +
            "HAVING COUNT(*) = 3"
            );

            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int x = resultSet.getInt("x");
                int y = resultSet.getInt("y");
                aliveCoordinates.add(new SimpleEntry<>(x, y));
            }

            // Mettre à jour la table "grid" pour mettre toutes les cellules à false
            preparedStatement = connection.prepareStatement("UPDATE grid SET state = false");
            preparedStatement.executeUpdate();

            // On met les cellles vivantes à true
            for (SimpleEntry<Integer, Integer> coordinate : aliveCoordinates) {
                int x = coordinate.getKey();
                int y = coordinate.getValue();
                preparedStatement = connection.prepareStatement("UPDATE grid SET state = true WHERE x = ? AND y = ?");
                preparedStatement.setInt(1, x);
                preparedStatement.setInt(2, y);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) { e.printStackTrace(); connection.rollback();}
    } 

    public void getRle(int session, List<CellEntity> RLE) {
        Connection connection = _Connector.getConnection(session);
        try {
            for(CellEntity cell : RLE) {
                PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE grid SET state=true WHERE x=? AND y=?"
                );
                preparedStatement.setInt(1,cell.getX());
                preparedStatement.setInt(2,cell.getY());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }



    public void emptyGrid(int session) throws SQLException {
        Connection connection = _Connector.getConnection(session);
        try {
            PreparedStatement prepareStatement = connection.prepareStatement(
                "UPDATE grid SET state = ? WHERE state = ?;"
            );
            prepareStatement.setBoolean(1, false);
            prepareStatement.setBoolean(2, true);
            prepareStatement.executeUpdate();
        } catch (SQLException e) { e.printStackTrace(); connection.rollback();}
        
    }
}