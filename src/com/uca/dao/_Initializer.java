package com.uca.dao;

import java.sql.*;

public class _Initializer {
    // nom de la table contenant la grille
    final static String TABLE = "grille";
    // taille de grille
    final static int SIZE = 100;

    /**
     * cette méthode permet d'initialiser en créant une table pour la grille si elle n'existe pas
     */
    public static void Init(){
        Connection connection = _Connector.getMainConnection();

        try {
            PreparedStatement statement;
            // statement = connection.prepareStatement("DROP TABLE IF EXISTS grid");
            // statement.executeUpdate();
            statement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS grid (x int, y int, state boolean, primary key(x, y));");
            statement.executeUpdate();

            statement = connection.prepareStatement("SELECT COUNT(*) FROM grid");
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            int count = resultSet.getInt(1);
            if (count == 0) {
                for(int x = 1; x <= SIZE; x++) {
                    for(int y = 1; y <= SIZE; y++) {
                        statement = connection.prepareStatement("INSERT INTO grid VALUES (?, ?, ?)");
                        statement.setInt(1, x);
                        statement.setInt(2, y);
                        statement.setBoolean(3, false);
                        statement.executeUpdate();
                    }
                    System.out.println(x);
                }
            }            
        } catch (SQLException e) { 
            System.out.println(e.toString());
            throw new RuntimeException("could not create database !");
        }
    }

    /**
     * teste si une table existe dans la base de données 
     */
    private static boolean tableExists(Connection connection, String tableName) throws SQLException {
        DatabaseMetaData meta = connection.getMetaData();
        ResultSet resultSet = meta.getTables(null, null, tableName, new String[]{"TABLE"});
        return resultSet.next();
    }
}
