package com.uca.dao;

import java.sql.*;
import java.util.List;

import com.uca.entity.CellEntity;
import com.uca.entity.GridEntity;
import com.uca.dao._Connector;

public class CellDAO {

    public void changeStateCell(int session, CellEntity cell) throws SQLException{
        Connection connection = _Connector.getConnection(session);
        try {
            PreparedStatement prepareStatement;
            prepareStatement = connection.prepareStatement(
                "SELECT state FROM grid WHERE x=? AND y=?;"
            );
            prepareStatement.setInt(1, cell.getX());
            prepareStatement.setInt(2, cell.getY());
            ResultSet resultSet = prepareStatement.executeQuery();
            resultSet.next();
            boolean state = resultSet.getBoolean("state");
            
            prepareStatement = _Connector.getConnection(session).prepareStatement(
                "UPDATE grid SET state=? WHERE x=? AND y=?;"
            );
            prepareStatement.setBoolean(1, !state);
            prepareStatement.setInt(2, cell.getX());
            prepareStatement.setInt(3, cell.getY());
            prepareStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback(); 
        } 
    }
}