package com.uca.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class _Connector {

    private static String url = "jdbc:postgresql://localhost/life";
    private static String user = "admin";
    private static String passwd = "admin";

    // private static Connection connect;
    private static HashMap<Integer, Connection> connections = new HashMap<>();
    // private static HashMap<Integer, Boolean> sessionLocks = new HashMap<>();


    public static Connection getMainConnection(){
        if(!connections.containsKey(0)) {
            connections.put(0, getNewConnection());
        }
        return connections.get(0);
    }

    public static Connection getConnection(int session) {
        if(!connections.containsKey(session)) {
            connections.put(session, getNewConnection());
        }
        return connections.get(session);
    }

    private static Connection getNewConnection() {
        Connection c;
        try {
            c = DriverManager.getConnection(url, user, passwd);
            c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

            c.setAutoCommit(false);
        } catch (SQLException e) {
            System.err.println("Erreur en ouvrant une nouvelle connection.");
            throw new RuntimeException(e);
        }
        
        return c;
    }

    // public static boolean isSessionLocked(int session) {
    //     for (Map.Entry<Integer, Boolean> s : sessionLocks.entrySet()) {
    //         if (s.getKey() != session && s.getValue()) {
    //             // Une autre session est bloquée, renvoyer true
    //             return true;
    //         }
    //     }
    //     // Aucune autre session bloquée, renvoyer false
    //     return false;
    // }
    
    
    // public static void lockSession(int session) {
    //     sessionLocks.put(session, true);
    // }
    
    // public static void unlockSession(int session) {
    //     sessionLocks.put(session, false);
    // }
    
    
}
