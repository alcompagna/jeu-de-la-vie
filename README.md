# Le jeu de la vie pour plusieurs éditeurs

## Description

Dans le cadre de ma licence, je devais faire une application web qui simulait le jeu de la vie. Le but principal de ce projet était la gestion de conflits en base de données.  

J'ai pensé deux manières de faire :
- Niveau d'isolation SERIALIZABLE
- Niveau d'isolation READ_COMMITTED

Ma première approche était de mettre SERIALIZABLE pour qu'une seule session modifie la grille à la fois. De cette manière, aucun conflit n'est possible. Le gros problème de cette manière c'est que lorsqu'une session est connectée/en cours de transaction, les autres ne peuvent rien faire.

Le but de projet étant d'avoir un jeu de la vie collaboratif, j'ai adopté la seconde approche avec READ COMMITTED qui permet un jeu de la vie plus collaboratif puisque chaque session peut modifer l'état des cellules à son aise tant qu'elles ne modifient pas la même cellules. Les états de celulles son alors regoupées lors des commits.

Vous pouvez lancer l'application en local avec **./gradlew run** et en utilisant localhost:8081.




